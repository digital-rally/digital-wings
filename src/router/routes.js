/* eslint-disable */
const routes = [
  {
    path: "/",
    component: () => import("layouts/LoginLayout.vue"),
    children: [
      {
        path: "",
        component: () => {
          console.log("KOMMT HALTS MAUL");
          return import("pages/Login.vue");
        },
      },
    ],
  },
  {
    path: "/home",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/Index.vue") }],
  },
  {
    path: "/customers",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/Customers.vue") }],
  },
  {
    path: "/approvals",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Approvals.vue") },
      {
        name: "Approval",
        path: ":id",
        component: () => import("pages/CustomerApproval.vue"),
      },
    ],
  },
  // { path: "*", redirect: "/" }, //SONST FUNKT MSAL NICHT -> WEIL SONST HASTAG GREIF UND URL KAPUTT MACHT
  // Always leave this as last one,
  // but you can also remove it
  // {
  // path: "/:catchAll(.*)*",
  // component: () => import("pages/Error404.vue"),
  // },
];

export default routes;
