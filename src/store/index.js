/* eslint-disable */
import { store } from "quasar/wrappers";
import { createStore } from "vuex";
import birdstore from "./birdstore";
import * as msal from "@azure/msal-browser";

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    state() {
      return {
        msalConfig: {
          auth: {
            clientId: "5ecbe09e-3256-43fa-9d70-8d10c36f7a2d",
            authority:
              "https://login.microsoftonline.com/ed19a6be-6641-4a38-9a8f-ef2005241c4e",
            // authority: "https://login.microsoftonline.com/common", //! NICHT COMMON VERWENDEN
            redirectUri:
              "msauth://org.cordova.quasar.app/2b6sEmWZq%2F4rCan9TZ0d6h7iduU%3D",
            // postLogoutRedirectUri:
            //   "msauth://org.cordova.quasar.app/2b6sEmWZq%2F4rCan9TZ0d6h7iduU%3D",
            // navigateToLoginRequestUrl: false,
            // knownAuthorities: [
            //   "https://login.microsoftonline.com/ed19a6be-6641-4a38-9a8f-ef2005241c4e",
            //   "https://login.microsoftonline.com/common",
            //   "msauth://org.cordova.quasar.app/2b6sEmWZq%2F4rCan9TZ0d6h7iduU%3D",
            // ],
          }, //msauth://5ecbe09e-3256-43fa-9d70-8d10c36f7a2d
          // cache: {
          //   cacheLocation: "sessionStorage",
          //   // cacheLocation: "localStorage",
          //   storeAuthStateInCookie: true,
          // },
          system: {
            // allowRedirectInIframe: true,
            loggerOptions: {
              loggerCallback: (level, message, containsPii) => {
                if (containsPii) {
                  return;
                }
                switch (level) {
                  case msal.LogLevel.Error:
                    console.error(message);
                    return;
                  case msal.LogLevel.Info:
                    console.info(message);
                    return;
                  case msal.LogLevel.Verbose:
                    console.debug(message);
                    return;
                  case msal.LogLevel.Warning:
                    console.warn(message);
                    return;
                }
              },
            },
            asyncPopups: true,
          },
        },
        accessToken: "",
      };
    },
    mutations: {
      setAccessToken(state, token) {
        state.accessToken = token;
      },
    },
    modules: {
      birdstore,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  });

  return Store;
});
